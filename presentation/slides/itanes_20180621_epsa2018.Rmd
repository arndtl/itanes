---
title: "Is there an economic vote in referendums?\\newline \\large Citizens' usage of heuristics in referendum voting"
author: "Arndt Leininger"
institute: "Freie Universität Berlin"
date: "21 June 2018"
output: 
  beamer_presentation:
    theme: "default"
    colortheme: "default"
    fonttheme: "professionalfonts"
    keep_tex: true
    slide_level: 2
bibliography: ../itanes.bib
in_header: header.tex
---

## Summary

**Case:** 

- Italian constitutional referendum, 4 December 2016 
- a complex constitutional reform and strongly polarized campaign

**Data**: 
ITANES pre- and post-referendum cross-section (N = 3050)

**Findings:**

- strong correlation between subjective evaluations of the economy and vote choice
- stronger 'economic vote' for more knowledgeable respondents

**Implication**:

- Italian citizens used referendum as possibility to hold Renzi to account
- Where in a business cycle a referendum is held may determine outcomes

## Motivation

*What influences how voters vote in a referendum?*

- A constitutional referendum triggered by the government as a typical case of a national level referendum
- A complex reform proposal
    - abolition of symmetric bicameralism, reform of the Senate, elimination of Provinces and CNEL
- A polarized campaign
    - 'Sì': Partito Democratico, Nuovo Centrodestra, ..., Coldiretti, CISL
    - 'No': MoVimento 5 Stelle, Lega Nord, Forza Italia, ..., National
Association of Italian Partisans, CGIL
- A personalized campaign
    - Renzi promised to resign if the reform were rejected

## Literature review

**Can voters make informed decisions in a referendum?**

- Uninformed cued voters vote similarly to informed voters [@lupia_shortcuts_1994;@christin_interests_2002]
- More knowledgeable voters more likely to vote their opinion in Swiss referendums [@lanz_vote_2014] but also more likely to be ambivalent [@nai_cadillac_2014]
- Voters use simple heuristics to decide, e.g. status quo bias [@bowler_demanding_1998;@leduc_politics_2003;@clarke_heuristics_2017]

- Economic evaluations: 'Can we afford change?' [@bowler_demanding_1998] or 'Do we need change?' [@jenssen_personal_1998]

## Economic voting in referendums

- The economic vote:
    - voters punish a government for a bad economy 
    - and reward it for a good economy
- best available evidence for electoral accountability [@kayser_elusive_2014]
    - mixed evidence base on aggregate data
    - more consistent evidence based on survey data

>1. Referendums are an opportunity to punish a government and the state of the economy is a prime determinant of that
>2. Given the prominence of the government's position in a referendum and the state of the economy the economic vote is an easily accessible heuristic for referendum voting
>3. Voters who possess less factual knowledge to base their decision on should be particularly prone to resort to simple heuristics

## Hypotheses

**Did perceptions of the economic situation in Italy influence how citizens voted in the 2016 Constitutional referendum?**

**H1** The more negative a respondent's evaluation of the economic situation of the country the more likely it is that she voted 'no' in the referendum.

**H2** The less knowledge a respondent possesses about the referendum the stronger will be the correlation of economic evaluations and vote choice in the referendum.

## Research Design

**Data**

- ITANES pre- and post-referendum cross-section
- N = 3050
- items on vote choice, economic evaluations and government approval

**Method**

- Logistic regression: binary vote choice on economic evaluations and other covariates

## Results

\begin{figure}
\includegraphics[width=.5\textwidth]{../../figures/f_m1.pdf}
\includegraphics[width=.5\textwidth]{../../figures/f_m2.pdf}
\caption{The economic vote}
\end{figure}

## Results

\begin{figure}
\includegraphics[width=.5\textwidth]{../../figures/f_m4.pdf}
\includegraphics[width=.5\textwidth]{../../figures/f_mediation.pdf}
\caption{The economic vote controlling for government approval}
\end{figure}

## Discussion

\begin{figure}
\includegraphics[width=.5\textwidth]{../../figures/f_eco_soc}
%\includegraphics[width=.5\textwidth]{../../figures/f_eco_ego}
\caption{Respondents' economic evaluations}
\end{figure}

## Discussion

- Stronger correlation for more informed voters
    1. Conforming with more recent evidence [@clarke_heuristics_2017]
    2. Factual knowledge induces ambivalence
    3. ~~Better informed voters have more accurate assessment of Italy's economic woes~~
- Risk aversion? 
    - 4/5 of Italians open to constitutional reform
    - What is the status quo? Keeping constitution or Renzi in place?
- A 'most-likely case' for an economic vote
    - Rejection of H1 would have been strong evidence against economic vote
    - Next: 'least-likely cases' and comparative studies

## Conclusion

- A strongly politicized referendum vote could be explained through an electoral heuristic
- Clashes with normative ideals: referendums are prospective while the economic vote is retrospective
- Strong economic vote which is stronger among more knowledgeable respondents [cf. @clarke_heuristics_2017]
- Where in a business cycle a referendum is held is important


## Grazie mille!

![Renzi before the referendum](../../private/images/renzi.jpg)

# Appendix

## Tables

![](t_main.png)

# References

## References

\tiny