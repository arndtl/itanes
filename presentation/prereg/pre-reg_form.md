OSF Pre-registration
====================

# Study Information

## Title

Is there an economic vote in referendums?

## Authors

Arndt Leininger

## Research Questions

*Broad*

Do considerations beyond the issues at stake in a referendum influence how voters vote in a referendum?

*Specific*

Do perceptions of the economy (national or personal situation) influence how citizens vote in a referendum?

*Concrete*

Did perceptions of the economic situation in Italy influence how citizens voted in the 2016 Constitutional referendum?

I also include a draft abstract here to provide more context (Note that the abstract will change in the final paper although analyses presented in the paper will be the ones pre-registered here):

Referendums have the potential to provide citizens with more control over policy. Yet, many citizens may decide how to vote based on extraneous factors, with negative consequences for the quality and legitimacy of the decision. I analyze the relationship between individuals' assessment of the national as well as their own economic situation and their vote choice in the 2017 Italian constitutional reform based on a survey conducted by the Italian National Election Studies (ITANES) project. The economic vote, the idea that voters punish a government for a bad economy and reward it for a good economy, has been and continues to be a large and central literature in electoral studies. It is the best available evidence for electoral accountability, however, it is undesirable in referendums because these should be about substantive issues not the government's handling of the economy. The 2016 Italian constitutional referendum vote provides a unique opportunity to learn about referendum voting as well as the nature of the economic vote.  If an economic vote exists in a referendum this suggests that the economic vote may be more of a general decision making heuristic rather than a conscious decision to hold the government accountable. I expect that a negative evaluation of the economy is associated with a greater likelihood to vote and that this relationship is stronger for low information voters. Statistical analyses are pre-registered with the Open Science Framework and will be carried out once data are available.

## Hypotheses

H1: The more negative a respondent's evaluation of the economic situation of the country the more likely it is that she voted 'no' in the referendum.

H2: The less knowledge a respondent posseses about the referendum the stronger will be the correlation of economic evaluations and vote choice in the referendum.

# Sampling Plan

## Existing data

Registration prior to accessing the data

## Registration prior to accessing the data

The pre-registered study will be based on a survey on the 2016 Italian constitutional referendum fielded by the Italian National Election Studies project (ITANES). The data will be made publicly available by ITANES on February 8, 2017 (expected date). This study has been pre-registered before this date and the author of this study is not affiliated in any way with ITANES.

## Data collection procedures

The data was collected by ITANES in a two-wave panel survey (pre- and post-referendum) of the Italian population.

## Sample size

I do not have any information on the sample size because the data have not been released yet. I expect the data to comprise about a 1,000 respondents.

## Sample size rationale

The sample size was determined by ITANES.

## Stopping rule

Does not apply.

# Variables

## Manipulated variables

Not applicable as this study is observational and data collection was carried out by a third party.

## Measured variables

I attach the questionnaire in Italian and my own translation using Google Translate.

I will use the following items from the pre-referendum questionnaire.

- S1 Gender:
  1) Male
  2) Female
- S2 Year of birth:
  -YYYY
- S5 Zone:
  1) North West,
  2) North East,
  3) Center,
  4) South,
  5) islands
- S10 Education:
  1) Elementary / untitled
  2) lower secondary
  3) higher in progress
  4) Professional Institute Diploma (3 years)
  5) a degree of maturity (5 years)
  6) A university in progress / no degree awarded
  7) University diploma / bachelor
  8) three-year BSc
  9) degree II level or degree 4-5 years
  10) master / postgraduate school
  11) Ph.D.
- S18 Evaluation of the national economy in the last year:
  - much improved
  - improved enough
  - remained equal
  - little worse
  - much worse
  - I do not know
- S20 Approval of the government: "How do you assesses the work of the government led by Matteo Renzi, in a scale of 0 to 10?"
  - 0 = completely negative judgment
  - 1
  - 2
  - 3
  - 4
  - 5
  - 6
  - 7
  - 8
  - 9
  - 10 = totally positive view
  - I do not know
- S28 if she intends to participate how she will vote
  - Yes, for the reform
  - No, against the reform
  - I have not decided yet
  - I prefer not to answer
- S30 to S32 questions testing knowledge about referendum:
  - S30 "What is the quorum (participation threshold) is necessary in order that the October referendum to be valid?"
    - Half (50%) of voters
    - Two-thirds (66%) of voters
    - There is no threshold: the referendum will be valid whatever the number of voters
  - S31 "The constitutional reform provides that"
    - The senators are not elected directly by voters more ù
    - A reduction of the members of the Chamber of Deputies
    - Abolition of the Senate and a Parliament with a single Chamber
  - S32 The constitutional reform provides for:
    - Abolish the CNEL
    - Abolish the Ministry of Education
    - Abolish the Cassa Depositi e Prestiti

I will use the following items from the post-referendum questionnaire.

- D1 Evaluation of the national economy in the last year: "Speaking of the economy. Do you think the economic situation in Italy in the last year"
  1) much improved
  2) improved enough
  3) remained equal
  4) enough worsened
  5) much worse
  6) I do not know
- D2 Evaluation of the economic situation of the respondent's family in the last year: "And the economic situation of your family:"
  1) much improved
  2) improved enough
  3) remained equal
  4) enough worsened
  5) much worse
  6) I do not know
- D5 Approval of the government: "How do you assess the work of the government led by Matteo Renzi? Expresses his opinion on a scale from 0 to 10 where 0 = 'completely negative judgment' 10 = 'completely positive judgment'."
  1) 0 = completely negative judgment
  2) 1
  3) 2
  4) 3
  5) 4
  6) 5
  7) 6
  8) 7
  9) 8
  10) 9
  11) 10 = totally positive view
  12) I do not know
- D28: Vote choice in the referendum: "How have you voted in the referendum on December 4?"
    1) I have voted No
    2) I voted for the Yes
    3) I voted white card
    4) I did not vote

## Indices

Additive index of knowledge about the referendum based on factual questions in post-referendum questionnaire.

know = S30 + S31 + S32 (where any of the items will be recoded to be == 1 if the question has been answered correctly, and 0 if otherwise)

Hence, the index will range from 0 (no correct answer) to 3 (three correct answers).

# Design Plan

## Study type

Observational Study

## Blinding

No blinding is involved in this study.

## Study design

This study is an analysis of cross-sectional survey data using logistic regression.

The sample will be restricted to voters who voted and chose either YES or NO. Data on respondents who indicate to be non-voters or voters casting a blank ballot will be discarded for the analysis. The dependent variable is 1 if the respondent voted YES and 0 if he or she voted NO.

Listwise deletion will be used in case of missing values in any of the independent variables.

## Randomization

There is no randomization involved in the study.

## Statistical models

Binary logistic regression will be used throughout. The dependent variable is a dummy variable indicating whether respondents voted in favor of the proposed constitutional reform or against it. In the following I am using R formulae expressions to detail the models.

I estimate models which capture retrospective sociotropic economic voting (using variable eco_soc, based on S18)

The dependent variable is a respondent's vote choice. This is measured as vote intention in the pre-referendum wave and as recall question in the post-referendum wave. Both measures are not without problems. As the item in the pre-referendum questionnaire is concerned: vote intentions may change and some respondents may voice a vote intention although they will not participate. As concerns the post-referendum questionnaire: there will almost certainly be over-reporting of participation and likely bandwaggoning. The extend to which this creates bias is hard to tell. A comparison of aggregate results with the actual referendum outcome is only possible once the data are available. I expect vote intentions to be more inaccurate and hence rely on reported vote choice post-referendum. Vote intentions are used as robustness check in alternative specifications to be reported in an appendix.

The main independent variable is a respondent's retrospective evaluation of the national economy. I rely on the item from the pre-referendum questionnaire. The same question is also asked in the post-referendum wave but here answers may be colored by media reporting about the economic consequences of the vote. I will report a specification with the item from the post-referendum wave in the appendix.

Respondents' evaluation of their own economic situation is only available in the post-referendum questionnaire. Models employing this variable will be presented in the appendix.

The knowledge index is build from the items in the pre-referendum questionnaire because answers to similar items in the post-referendum questionnaire may be informed by media reporting after the referendum.

**Main Text**

The results of the following models will be presented in the main text. In a first specification I regress the vote choice on a respondent's economic evaluation. This specification tests hypothesis 1. In a second specification I interact a respondent's knowledge about the referendum with his or her economic evaluation. This specification tests hypothesis 2.

1. vote ~ eco_soc + female + age + uni + north + center + islands
2. vote ~ eco_soc * know + approval + female + age + uni + north + center + islands

**Appendix**

In the appendix I first present as a Robustness test of the main results alternative specifications with vote intentions (vote_pre) and economic evaluations from the post-referendum questionnaire (eco_soc_post).

*Robustness*

1. vote_pre ~ eco_soc + female + age + uni + north + center + islands
2. vote_pre ~ eco_soc * know + approval + female + age + uni + north + center + islands

1. vote ~ eco_soc_post + female + age + uni + north + center + islands
2. vote ~ eco_soc_post * know + approval + female + age + uni + north + center + islands

*Mechanism*

I have chosen respondents' retrospective economic evaluations because this is a factor which should be outside considerations which should reasonably inform vote choice in a referendum. As Italian prime minister Renzi, the principal author of the constitution reforms, tied his political future to the referendum government approval can be less credibly viewed as external to the referendum. However, approval of the government may be a mediating factor, influenced by economic evaluations and influencing an individual's vote choice.
To trace that mechanism, I, In a third specification, regress government approval on economic evaluations. Finally, in a fourth specification I regress vote choice on economic evaluations and government approval, following the procedure proposed by Baron and Kenny (1986). I expect the coefficient on economic evaluations to shrink vis-a-vis the first specification.

3. approval ~ eco_soc + female + age + uni + north + center + islands
4. vote ~ eco_soc + approval + female + age + uni + north + center + islands

*Egotropic economic voting*

I also estimate the same models with an alternative independent variable which captures retrospective egotropic economic voting (using variable eco_ego, based on D1). The literature on economic voting almost unanimously suggests that sociotropic voting is stronger than egotropic voting. Nevertheless, it is interesting to explore whether this holds for referendums as well. Hence, I presenting models testing for egotropic economic voting in the appendix.

1. vote ~ eco_ego + female + age + uni + north + center + islands retrospective egotropic economic voting
2. vote ~ eco_ego * know + approval + female + age + uni + north + center + islands

3. approval ~ eco_ego + female + age + uni + north + center + islands retrospective egotropic economic voting
4. vote ~ eco_ego + approval + female + age + uni + north + center + islands retrospective egotropic economic voting

The same control variables are included in all specifications. The coding of variables is explained in the next subsection.

Baron, R. M. and Kenny, D. A. (1986) "The Moderator-Mediator Variable Distinction in Social Psychological Research - Conceptual, Strategic, and Statistical Considerations", Journal of Personality and Social Psychology, Vol. 51(6), pp. 1173–1182.

## Transformations

- vote: recode of D28
  - 1 = 2 (Yes)
  - 0 = 1 (No)
  - NA = 3 (white card) or 4 (did not vote)
- vote_pre: recode of S28
  - 1 = Yes, for the reform
  - 0 = No, against the reform
  - NA = I have not decided yet and I prefer not to answer
- eco_soc: recode of S18
  - 2 = much improved
  - 1 = improved enough
  - 0 = remained equal
  - -1 = little worse
  - -2 = much worse
  - NA = I do not know
- eco_soc_post: recode of D1
  - keep all values but 6
  - NA = 6
- eco_ego: recode of D2
  - keep all values but 6
  - NA = 6
- approval: recode of S20
  - keep all values except the following
  - NA = I do not know
- female
  - substract 1 so that
  - 1 = 2 (Female)
  - 0 = 1 (Male)
- age: calculate age based on S2 (year of birth)
  - age = 2016 - S2
- uni: recode of S10
  - 1 if 7, 8, 9, 10 or 11 (Bachelor or higher)
  - 0 otherwise (no university degree, includes university students)
- north: dummy based on S5
  - 1 if S5 == 1 (North West) or S5 == 2 (North East)
  - 0 otherwise
- center: dummy based on S5
  - 1 if S5 == 3 (Center)
  - 0 otherwise
- islands
  - 1 if S5 == 5 (islands)
  - 0 otherwise
  - The reference category for these regional dummies is S5 == 4 (South)
- know: index indicating knowledge about the referendum ranging from 1 to 3
  - explained in subsection "Indices"

## Follow-up analyses

None.

## Follow-up analyses

Null Hypothesis Significance Testing with standard significance levels (p < .05) will be used.

## Data exclusion

The analysis will focus on voters in the referendum only. Non-voters as well as voters casting a blank ballot will be ignored.

## Missing data

List-wise deletion will be used in case of missing values on the variables to be analysed.

## Exploratory analysis

None.

# Scripts

## Upload an analysis script with clear comments

None to be uploaded.

# Other

## Other

N/A
