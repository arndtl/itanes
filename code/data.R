# Economic Voting in Referendums
# created: 2017-05-12
# updated: 2018-12-07

setwd('~/Git/itanes')

library(foreign)
library(dplyr)
library(stringr)

d <- read.dta('private/data/ITANES_POSTREFERENDUM_2016_COS_COMPETITION_saveold.dta') %>%
  as_tibble()

# recode and rename relevant variables -----------------------------------------

df <-
  d %>% mutate(# pre-referendum wave
    gender = recode(s01, 'Uomo' = 'male', 'Donna' = 'female'),
    female = gender == 'female',
    yearbirth = s03,
    age = 2016 - yearbirth,
    agecohort = ordered(s02),
    education =
      recode(s10, "diploma di maturita' (5 anni)" = 'a degree of maturity (5 years)',
             "laurea triennale di I livello" = 'three-year BSc',
             "laurea specialistica di II livello o laurea 4-5 anni" = 'degree II level or degree 4-5 years',
             "universita' in corso/nessuna laurea conseguita" = 'A university in progress / no degree awarded',
             "media inferiore" = 'lower secondary',
             "master/scuola di specializzazione post laurea" = '',
             "diploma di istituto professionale (3 anni)" = 'master / postgraduate school',
             "diploma universitario/laurea breve" = 'University diploma / bachelor',
             "superiori in corso" = 'higher in progress',
             "elementare/privo di titolo" = 'Elementary / untitled',
             "dottorato di ricerca" = 'Ph.D.'),
    uni = education == 'University diploma / bachelor' |
      education == 'three-year BSc' |
      education == 'degree II level or degree 4-5 years' |
      education == 'master / postgraduate school' |
      education == 'Ph.D.',
    # zone = s05, # only one value: 99
    region = s04,
    ecosoc_pre = recode(S18, 'Rimasta eguale' = 'remained equal',
                        'Abbastanza migliorata' = 'improved enough',
                        'Abbastanza peggiorata' = 'little worse',
                        'Molto peggiorata' = 'much worse',
                        'Molto migliorata' = 'much improved',
                        'Non saprei' = as.character(NA)),
    ecosoc_pre = ordered(ecosoc_pre, levels =
                           c('much worse', 'little worse', 'remained equal',
                             'improved enough', 'much improved')),
    eco_soc = as.numeric(ecosoc_pre),
    govapproval_pre = recode(S20, '0=giudizio completamente negativo' =
                               '0 = completely negative judgment',
                             '10=giudizio completamente positivo' =
                               '10 = completely positive jdugment',
                             'Non saprei' = as.character(NA)),
    approval = as.numeric(str_extract(govapproval_pre, '\\d+')),
    # govapproval_pre = ordered(govapproval_pre, levels = c('')),
    opinion = as.numeric(str_extract(S26, '\\d+')),
    undecided = opinion == 5 | S26 == 'Non saprei',
    voteintention = recode(S28, 'Sì a favore della riforma' = 'Yes, for the reform',
                           'No, contro la riforma' = 'No, against the reform',
                           'Non ho ancora deciso' = 'I have not decided yet',
                           'Preferisco non rispondere' = 'I prefer not to answer'),
    vote_pre = ifelse(voteintention == 'Yes, for the reform', 1,
                      ifelse(voteintention == 'No, against the reform', 0, NA)),
    know1 = recode(S30, 'La meta (50%) degli elettori' =
                     'Half (50%) of voters',
                   'I due terzi (66%) degli elettori' =
                     'Two-thirds (66%) of voters',
                   'Non c è una soglia il referendum sarà valido qualunque sia il numero di votant' = 'There is no threshold: the referendum will be valid whatever the number of voters'),
    know1_correct = know1 == 'There is no threshold: the referendum will be valid whatever the number of voters',
    know2 = recode(S31, 'I senatori non siano più eletti direttamente dagli elettori' =
                     'Senators are no longer elected directly by voters',
                   'Una riduzione dei membri della Camera dei Deputati' =
                     'A reduction of the members of the Chamber of Deputies',
                   'L abolizione del senato e un Parlamento con una sola Camera' =
                     'Abolition of the Senate and a Parliament with a single Chamber'),
    know2_correct = know2 == 'Senators are no longer elected directly by voters',
    know3 = recode(S32, 'Abolire il CNEL' = 'Abolish the CNEL',
                   'Abolire il MIUR' = 'Abolish the Ministry of Education',
                   'Abolire la Cassa Depositi e Prestiti' =
                     'Abolish the Cassa Depositi e Prestiti'),
    know3_correct = know3 == 'Abolish the CNEL',
    know = know1_correct + know2_correct + know3_correct,
    vote_party = S15,
    vote_gov = ifelse(vote_party %in% c('Partito Democratico (Pd)',
                                        'Unione Di Centro',
                                        'Scelta Civica Con Monti Per L Italia'),
                      1,
                      ifelse(vote_party %in% c('Ho votato scheda Bianca/Nulla',
                                               'Preferisco non rispondere'),
                             NA, 0)),
    approval_renzi = as.numeric(str_extract(S25_1, '\\d+')),
    # post-referendum wave
    ecosoc_post = recode(d$D1_W9, 'rimasta eguale' = 'remained equal',
                         'abbastanza migliorata' = 'improved enough',
                         'abbastanza peggiorata' = 'little worse',
                         'molto peggiorata' = 'much worse',
                         'non saprei' = as.character(NA),
                         'molto migliorata' = 'much improved'),
    ecosoc_post = ordered(ecosoc_post,
                          levels = c('much worse', 'little worse', 'remained equal',
                                     'improved enough', 'much improved')),
    eco_soc_post = as.numeric(ecosoc_post),
    ecoego_post = recode(D2_W9, 'rimasta eguale' = 'remained equal',
                         'abbastanza migliorata' = 'improved enough',
                         'abbastanza peggiorata' = 'little worse',
                         'molto peggiorata' = 'much worse',
                         'non saprei' = as.character(NA),
                         'molto migliorata' = 'much improved'),
    ecoego_post = ordered(ecoego_post, levels =
                            c('much worse', 'little worse', 'remained equal',
                              'improved enough', 'much improved')),
    eco_ego = as.numeric(ecoego_post),
    govapproval_post = recode(D5_W9, '0&nbsp;=giudizio completamente negativo' =
                                '0 = completely negative judgment',
                              '10&nbsp;=giudizio completamente positivo' =
                                '10 = completely positive jdugment',
                              'non saprei' = "don't know"),
    votechoice = recode(D28_W9, 'ho votato No' = 'No', "ho votato Si'" = 'Yes',
                        'ho votato scheda bianca' = 'blank',
                        'non ho votato' = 'did not vote'),
    vote = ifelse(votechoice == 'Yes', 1, ifelse(votechoice == 'No', 0, NA)),
    pid = as.character(D11_W9),
    pid = ifelse(pid != 'preferisco non rispondere', pid, NA),
    pid_gov = pid %in% c('Partito Democratico',
                         'Area Popolare/Nuovo Centrodestra-UDC',
                         "Scelta Civica/Cittadini per l'Italia (include ALA)"),
    pid_none = pid == 'nessun partito',
    pid_3 = ifelse(pid_gov == T, 'government', ifelse(pid_none == T, 'none',
                                                      'opposition')),
    pid_opp = pid_3 == 'opposition',
    lr = as.numeric(str_extract(D9_01_W9, '\\d+')),
    lr_pd = as.numeric(str_extract(D10_01_W9, '\\d+')),
    lr_dist_pd = abs(lr - lr_pd)
  )

df <- df %>% select(gender, female, yearbirth, contains('age'), education, uni,
                    region,
                    contains('eco'), contains('approval'), opinion, undecided,
                    contains('vote'), contains('know'), contains('pid'),
                    contains('lr'))

# save data --------------------------------------------------------------------

write.csv(df, 'private/data/data.csv', row.names = F)

# descriptives -----------------------------------------------------------------

# df <- read.csv('private/data/data.csv', stringsAsFactors = F)

# sample size

# nrow(df)