# Economic Voting in Referendums
# created: 2017-05-12
# updated: 2017-06-06

# rm(list = ls())

setwd('~/Git/itanes')

library(dplyr)
library(ggplot2)
library(interplot)
library(extrafont)
loadfonts()

df <- read.csv('private/data/data.csv', stringsAsFactors = F) %>% tbl_df()

load('models/models.RData')

# manuscript -------------------------------------------------------------------

# m1

dfig <- data.frame(eco_soc = 1:5, know = 2, female = TRUE,
                      age = median(df$age, na.rm = T),
                      uni = T)

dfig$prediction <- predict(m1, newdata = dfig, type = 'response')
dfig$se_fit <- predict(m1, newdata = dfig, type = 'response', se.fit = T)$se.fit
dfig$lwr <- dfig$prediction - 1.96 * dfig$se_fit
dfig$upr <- dfig$prediction + 1.96 * dfig$se_fit

dfig_m1 <- dfig

f_m1 <-
ggplot(dfig, aes(x = eco_soc, y = prediction, ymin = lwr, ymax = upr)) +
  geom_errorbar(width = 0) +
  geom_point(size = 2) +
  scale_y_continuous(breaks = seq(.1, .9, .2)) +
  ylab('Probability to vote yes') + xlab('Sociotropic economic evaluation') +
  theme_bw()

pdf('figures/f_m1.pdf', width = 4, height = 4)
f_m1 + theme(text = element_text(family ='CMU Serif', size = 11))
dev.off()

png('figures/f_m1.png', width = 400, height = 400)
f_m1 + theme(text = element_text(family ='CMU Serif', size = 11))
dev.off()

# # m2 (Interaction model (main text))
#
# f_m2 <-
#   interplot(m2, 'eco_soc', 'know', hist = T, point = T) +
#   xlab('Referendum knowledge') + ylab('Marginal effect') +
#   scale_y_continuous(breaks = seq(0,1.5, .5)) +
#   theme_bw()
#
# pdf('figures/f_m2.pdf', width = 3, height = 3)
# f_m2 + theme(text = element_text(family ='CMU Serif', size = 12))
# dev.off()

# # Model 3 in table in manuscript
#
# dfig <- data.frame(eco_soc = 1:5, know = 2, female = TRUE,
#                    age = median(df$age, na.rm = T),
#                    uni = T)
#
# dfig$prediction <- predict(m3, newdata = dfig, type = 'response')
# dfig$se_fit <- predict(m3, newdata = dfig, type = 'response', se.fit = T)$se.fit
# dfig$lwr <- dfig$prediction - 1.96 * dfig$se_fit
# dfig$upr <- dfig$prediction + 1.96 * dfig$se_fit
#
# f_m3 <-
#     ggplot(dfig, aes(x = eco_soc, y = prediction, ymin = lwr, ymax = upr)) +
#     geom_errorbar(width = 0) +
#     geom_point(size = 2) +
#     scale_y_continuous(breaks = seq(2, 8, 2)) +
#     ylab('Government approval') + xlab('Sociotropic economic evaluation') +
#     theme_bw()
#
# pdf('figures/f_m3.pdf', width = 3, height = 3)
# f_m3 + theme(text = element_text(family ='CMU Serif', size = 12))
# dev.off()

# Model 2 in table in manuscript
dfig <- data.frame(eco_soc = 1:5, know = 2, female = TRUE,
                   age = median(df$age, na.rm = T),
                   uni = T, pid_gov = F)

dfig$prediction <- predict(m2, newdata = dfig, type = 'response')
dfig$se_fit <- predict(m2, newdata = dfig, type = 'response', se.fit = T)$se.fit
dfig$lwr <- dfig$prediction - 1.96 * dfig$se_fit
dfig$upr <- dfig$prediction + 1.96 * dfig$se_fit

dfig_m2 <- dfig

f_m2 <-
    ggplot(dfig, aes(x = eco_soc, y = prediction, ymin = lwr, ymax = upr)) +
    geom_errorbar(width = 0) +
    geom_point(size = 2) +
    scale_y_continuous(breaks = seq(.1,.9,.2)) +
    ylab('Probability to vote yes') + xlab('Sociotropic economic evaluation') +
    theme_bw()

pdf('figures/f_m2.pdf', width = 3, height = 3)
f_m2 + theme(text = element_text(family ='CMU Serif', size = 12))
dev.off()

# Model 3 in table in manuscript
dfig <- data.frame(eco_soc = 1:5, know = 2, female = TRUE,
                   age = median(df$age, na.rm = T),
                   uni = T, vote_gov = 0)

dfig$prediction <- predict(m3, newdata = dfig, type = 'response')
dfig$se_fit <- predict(m3, newdata = dfig, type = 'response', se.fit = T)$se.fit
dfig$lwr <- dfig$prediction - 1.96 * dfig$se_fit
dfig$upr <- dfig$prediction + 1.96 * dfig$se_fit

dfig_m3 <- dfig

f_m3 <-
    ggplot(dfig, aes(x = eco_soc, y = prediction, ymin = lwr, ymax = upr)) +
    geom_errorbar(width = 0) +
    geom_point(size = 2) +
    scale_y_continuous(breaks = seq(.1,.9,.2)) +
    ylab('Probability to vote yes') + xlab('Sociotropic economic evaluation') +
    theme_bw()

pdf('figures/f_m3.pdf', width = 3, height = 3)
f_m3 + theme(text = element_text(family ='CMU Serif', size = 12))
dev.off()

# Model 4 in table in manuscript
dfig <- data.frame(eco_soc = 1:5, know = 2, female = TRUE,
                   age = median(df$age, na.rm = T),
                   uni = T, approval = 5)

dfig$prediction <- predict(m4, newdata = dfig, type = 'response')
dfig$se_fit <- predict(m4, newdata = dfig, type = 'response', se.fit = T)$se.fit
dfig$lwr <- dfig$prediction - 1.96 * dfig$se_fit
dfig$upr <- dfig$prediction + 1.96 * dfig$se_fit

dfig_m4 <- dfig

f_m4 <-
  ggplot(dfig, aes(x = eco_soc, y = prediction, ymin = lwr, ymax = upr)) +
  geom_errorbar(width = 0) +
  geom_point(size = 2) +
  ylab('Probability to vote yes') + xlab('Sociotropic economic evaluation') +
  theme_bw()

pdf('figures/f_m4.pdf', width = 3, height = 3)
f_m4 + theme(text = element_text(family ='CMU Serif', size = 12))
dev.off()


# Model 5 in table in manuscript
dfig <- data.frame(eco_soc = 1:5, know = 2, female = TRUE,
                   age = median(df$age, na.rm = T),
                   uni = T, approval_renzi = 5)

dfig$prediction <- predict(m5, newdata = dfig, type = 'response')
dfig$se_fit <- predict(m5, newdata = dfig, type = 'response', se.fit = T)$se.fit
dfig$lwr <- dfig$prediction - 1.96 * dfig$se_fit
dfig$upr <- dfig$prediction + 1.96 * dfig$se_fit

dfig_m5 <- dfig

f_m5 <-
    ggplot(dfig, aes(x = eco_soc, y = prediction, ymin = lwr, ymax = upr)) +
    geom_errorbar(width = 0) +
    geom_point(size = 2) +
    ylab('Probability to vote yes') + xlab('Sociotropic economic evaluation') +
    theme_bw()

pdf('figures/f_m5.pdf', width = 3, height = 3)
f_m5 + theme(text = element_text(family ='CMU Serif', size = 12))
dev.off()



dfig_m1_m4 <- bind_rows(dfig_m1, dfig_m4) %>%
    mutate(model = ifelse(is.na(approval), '(1)', '(2)'))

f_m1_m4 <-
    ggplot(dfig_m1_m4, aes(x = eco_soc, y = prediction, ymin = lwr, ymax = upr)) +
    geom_errorbar(width = 0) +
    geom_point(size = 2) + facet_wrap(~model) +
    scale_y_continuous(breaks = seq(.1, .9, .2)) +
    ylab('Probability to vote yes') + xlab('Sociotropic economic evaluation') +
    theme_bw() + theme(strip.background = element_rect(fill = 'white'))

pdf('figures/f_m1_m4.pdf', width = 6, height = 3)
f_m1_m4 + theme(text = element_text(family ='CMU Serif', size = 12))
dev.off()


# Figure for PID groups

dfig <- data.frame(eco_soc = 1:5, know = 2, female = TRUE,
                   age = median(df$age, na.rm = T),
                   uni = T)

dfig$prediction <- predict(m1_pidg, newdata = dfig, type = 'response')
dfig$se_fit <- predict(m1, newdata = dfig, type = 'response', se.fit = T)$se.fit
dfig$lwr <- dfig$prediction - 1.96 * dfig$se_fit
dfig$upr <- dfig$prediction + 1.96 * dfig$se_fit

dfig_m1_pidg <- dfig %>% mutate(model = 'PID (Government)')

dfig$prediction <- predict(m1_pidn, newdata = dfig, type = 'response')
dfig$se_fit <- predict(m1, newdata = dfig, type = 'response', se.fit = T)$se.fit
dfig$lwr <- dfig$prediction - 1.96 * dfig$se_fit
dfig$upr <- dfig$prediction + 1.96 * dfig$se_fit

dfig_m1_pidn <- dfig %>% mutate(model = 'PID (None)')

dfig$prediction <- predict(m1_pido, newdata = dfig, type = 'response')
dfig$se_fit <- predict(m1, newdata = dfig, type = 'response', se.fit = T)$se.fit
dfig$lwr <- dfig$prediction - 1.96 * dfig$se_fit
dfig$upr <- dfig$prediction + 1.96 * dfig$se_fit

dfig_m1_pido <- dfig %>% mutate(model = 'PID (Opposition)')

dfig <- bind_rows(dfig_m1_pidg, dfig_m1_pidn, dfig_m1_pido)


f_models1 <-
    ggplot(dfig, aes(x = eco_soc, y = prediction, ymin = lwr, ymax = upr)) +
    geom_errorbar(width = 0) +
    geom_point(size = 2) +
    scale_y_continuous(breaks = seq(.1, .9, .2)) +
    facet_wrap(~model) +
    ylab('Probability to vote yes') + xlab('Sociotropic economic evaluation') +
    theme_bw() + theme(strip.background = element_rect(fill = 'white'))

pdf('figures/f_models1.pdf', width = 6, height = 3)
f_models1 + theme(text = element_text(family ='CMU Serif', size = 12))
dev.off()

png('figures/f_models1.png', width = 600, height = 300)
f_models1 + theme(text = element_text(family ='CMU Serif', size = 12))
dev.off()


# appendix ---------------------------------------------------------------------

# Vote intentions --

# ma1_v
dfig <- data.frame(eco_soc = 1:5, know = 2, female = TRUE,
                   age = median(df$age, na.rm = T),
                   uni = T)

dfig$prediction <- predict(ma1_v, newdata = dfig, type = 'response')
dfig$se_fit <- predict(ma1_v, newdata = dfig, type = 'response', se.fit = T)$se.fit
dfig$lwr <- dfig$prediction - 1.96 * dfig$se_fit
dfig$upr <- dfig$prediction + 1.96 * dfig$se_fit

f_ma1_v <-
  ggplot(dfig, aes(x = eco_soc, y = prediction, ymin = lwr, ymax = upr)) +
  geom_errorbar(width = 0) +
  geom_point() +
  ylab('Probability to vote yes') + xlab('Sociotropic economic evaluation') +
  theme_bw()

pdf('figures/f_ma1_v.pdf')
f_ma1_v + theme(text = element_text(family ='CMU Serif', size = 22),
                   plot.title = element_text(hjust = 0.5)) #+
  # ggtitle('Vote intention')
dev.off()

# ma2_v (Interaction model (appendix -- robustness))

f_ma2_v <-
  interplot(ma2_v, 'eco_soc', 'know', hist = T, point = T) +
  xlab('Referendum knowledge') + ylab('Marginal effect') +
  scale_y_continuous(breaks = seq(0,1.5, .5)) +
  theme_bw()

pdf('figures/f_ma2_v.pdf')
f_ma2_v + theme(text = element_text(family ='CMU Serif', size = 22))
dev.off()

# Sociotropic economy measured post.referendum (eco_soc_post)

# ma1_e
dfig <- data.frame(eco_soc_post = 1:5, know = 2, female = TRUE,
                   age = median(df$age, na.rm = T),
                   uni = T)

dfig$prediction <- predict(ma1_e, newdata = dfig, type = 'response')
dfig$se_fit <- predict(ma1_e, newdata = dfig, type = 'response', se.fit = T)$se.fit
dfig$lwr <- dfig$prediction - 1.96 * dfig$se_fit
dfig$upr <- dfig$prediction + 1.96 * dfig$se_fit

f_ma1_e <-
  ggplot(dfig, aes(x = eco_soc_post, y = prediction, ymin = lwr, ymax = upr)) +
  geom_errorbar(width = 0) +
  geom_point() +
  ylab('Probability to vote yes') + xlab('Sociotropic economic evaluation') +
  theme_bw()

pdf('figures/f_ma1_e.pdf')
f_ma1_e + theme(text = element_text(family ='CMU Serif', size = 22),
                   plot.title = element_text(hjust = 0.5)) #+
  # ggtitle('Sociotropic economic evaluation\n (post referendum)')
dev.off()

# ma2_e

f_ma2_e <-
  interplot(ma2_e, 'eco_soc_post', 'know', hist = T, point = T) +
  xlab('Referendum knowledge') + ylab('Marginal effect') +
  scale_y_continuous(breaks = seq(0,1.5, .5)) +
  theme_bw()

pdf('figures/f_ma2_e.pdf')
f_ma2_e + theme(text = element_text(family ='CMU Serif', size = 22))
dev.off()

# me1
dfig <- data.frame(eco_ego = 1:5, know = 2, female = TRUE,
                   age = median(df$age, na.rm = T),
                   uni = T)

dfig$prediction <- predict(me1, newdata = dfig, type = 'response')
dfig$se_fit <- predict(me1, newdata = dfig, type = 'response', se.fit = T)$se.fit
dfig$lwr <- dfig$prediction - 1.96 * dfig$se_fit
dfig$upr <- dfig$prediction + 1.96 * dfig$se_fit

f_me1 <-
  ggplot(dfig, aes(x = eco_ego, y = prediction, ymin = lwr, ymax = upr)) +
  geom_errorbar(width = 0) +
  geom_point() +
  ylab('Probability to vote yes') + xlab('Sociotropic economic evaluation') +
  theme_bw()

pdf('figures/f_me1.pdf')
f_me1 + theme(text = element_text(family ='CMU Serif', size = 22),
              plot.title = element_text(hjust = 0.5))
dev.off()

# me2 (Interaction model (appendix -- Egotropic economic voting))

f_me2 <-
  interplot(me2, 'eco_ego', 'know', hist = T, point = T) +
  xlab('Referendum knowledge') + ylab('Marginal effect') +
  scale_y_continuous(breaks = seq(0,1.5, .5)) +
  theme_bw()

pdf('figures/f_me2.pdf')
f_me2 + theme(text = element_text(family ='CMU Serif', size = 22))
dev.off()
